import java.util.ArrayList;


public class Vertex {

    public Vertex( String name ) {
        this.name = name.charAt(0);
    }

    public char getName() {
        return name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Vertex vertex = (Vertex) o;
        if (name != vertex.name) return false;
        return true;
    }

    @Override
    public int hashCode() {
        return (int) name;
    }

    private char name;
}