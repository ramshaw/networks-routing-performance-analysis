import java.util.ArrayList;

/**
 * Created by aaron on 22/10/14.
 */
public class SearchAlgorithm {

    public SearchAlgorithm( Graph graph, ArrayList<ConnectionRequest> requests, String scheme, Integer packetRate, String algorithm) {
        this.graph = graph;
        this.requests = requests;
        this.scheme = scheme;
        this.packetRate = packetRate;
        this.algorithm = algorithm;
    }

    public String analyseWithAlgorithm() {
        String output = "";
        Graph graph = this.getGraph();
        int totalPackets = 0;
        float blockedPackets = 0;
        float successPackets = 0;
        float totalDelay = 0;
        float totalHops = 0;
        float successRequest = 0;
        int successCount = 0;

        if( scheme.equals("CIRCUIT") ) {
            int a = 0;
            for ( ConnectionRequest request: this.getRequests() ) {
                System.out.println(a);
                a++;
                totalPackets += Math.ceil(request.getDuration() * this.getPacketRate()) ;
                Vertex start = graph.getVertex(request.getStart());
                Vertex end = graph.getVertex(request.getEnd());
                ArrayList<Vertex> path = graph.dijkstra(start, end, request.getInitTime(), request.getDuration(), algorithm);
                if( path != null ) {
                    totalHops += path.size() -1;
                    successPackets += Math.ceil(request.getDuration() * this.getPacketRate());
                    totalDelay += graph.pathDelay(path );
                    successCount++;
                    successRequest++;
                } else {
                    blockedPackets += Math.ceil(request.getDuration() * this.getPacketRate());
                }
            }
        }
        else if( scheme.equals("PACKET") ) {
            int a = 0;
            for( ConnectionRequest request: this.getRequests() ) {
   //             graph.clean(request.getStart());
                System.out.printf("%d \n", a);
                a++;
                totalPackets += Math.ceil(request.getDuration() * this.getPacketRate()) ;
                int p = (int) Math.ceil (request.getDuration() * this.getPacketRate() );

                int hops = 0;
                float delay = 0;

                float packetTimeDuration;
                if ( 1.0/this.getPacketRate() > request.getDuration() ) packetTimeDuration = request.getDuration();
                else packetTimeDuration = (float) (1.0/this.getPacketRate());

                Vertex start = graph.getVertex(request.getStart());
                Vertex end = graph.getVertex(request.getEnd());

                float time = 0;

                for( int i = 1; i < p+1; i++ ) {

                    if( packetTimeDuration*i > request.getDuration() ) {
                        packetTimeDuration = request.getDuration() - ((p-1)* this.getPacketRate());
                    }
                    //System.out.println(request.getStart() + " " + request.getEnd() + " time " +  (request.getInitTime() + time) + " end " +  packetTimeDuration);
                    if( packetTimeDuration != 0 ) {
                        ArrayList<Vertex> path = graph.dijkstra(start, end, request.getInitTime() + time, packetTimeDuration, algorithm);

                        if (path != null) {
                            hops += path.size() - 1;
                            successPackets++;
                            delay += graph.pathDelay(path);
                            successCount++;
                            //     System.out.println("Success " + start.getName() + " " + end.getName() + " packet " + i + " of " + p);
                        } else {
                            //    System.out.println("Fail    " + start.getName() + " " + end.getName() + " packet " + i + " of " + p);

                            blockedPackets++;
                        }
                    }
                    time += packetTimeDuration;

                }

                successRequest++;
                totalHops += hops/p;
                delay = delay/p;
                totalDelay += delay;
            }

        }

        totalDelay = totalDelay / successRequest;
        int totalPacketsInt = (int) Math.ceil(totalPackets);
        int blockedPacketsInt = Math.round(blockedPackets);
        int successPacketsInt = Math.round(successPackets);

        totalHops = totalHops / (this.getRequests()).size();

        output += "total number of virtual circuit requests: " + this.getRequests().size() + "\n";
        output += "total number of packets: " + totalPackets + "\n";
        output += "number of successfully routed packets: " +(int) successPackets+ "\n";
        output += "percentage of successfully routed packets: " + String.format("%.2f", (1-(blockedPackets/successPackets))* 100 )+ "\n";
        output += "number of blocked packets: " + (int) blockedPackets + "\n";
        output += "percentage of blocked packets: " + String.format("%.2f", (blockedPackets/successPackets * 100) ) + "\n";
        output += "average number of hops per circuit: " + String.format("%.2f", totalHops) + "\n";
        output += "average cumulative propagation delay per circuit: " +totalDelay +"\n";
        return output;
    }


    public ArrayList<ConnectionRequest> getRequests() {
        return requests;
    }

    public String getScheme() {
        return scheme;
    }

    public Integer getPacketRate() {
        return packetRate;
    }

    public Graph getGraph() {
        return graph;
    }

    private Graph graph;
    private ArrayList<ConnectionRequest> requests;
    private String scheme;
    private Integer packetRate;
    private String algorithm;
}
