import java.io.*;
import java.util.ArrayList;

public class RoutingPerformance {

    public static void main(String[] args) {

        // Initialise lists of edges and connections
        requests = new ArrayList<ConnectionRequest>();
        File file = new File(args[2]);
        String line = null;
        Graph graph = new Graph();

        // Parse input from arg[2] which should be the topology file
        try {
            BufferedReader reader = new BufferedReader(new FileReader(file));
            while( (line = reader.readLine()) != null  ) {
                String[] elements = line.split(" ");
                graph.addEdge(elements[0], elements[1], elements[2], elements[3] );
            }
        }
        catch (FileNotFoundException e) {
            System.out.println("No topology found");
        } catch (IOException e) {
            System.out.println("IO exception");
        }
        file = new File(args[3]);

        // Parse input from arg[3] which should be workload file
        try {
            BufferedReader reader = new BufferedReader(new FileReader(file));
            while( (line = reader.readLine()) != null  ) {
                String[] elements = line.split(" ");
                ConnectionRequest r = new ConnectionRequest(elements[0], elements[1], elements[2], elements[3]);
                requests.add(r);
            }
        }
        catch (FileNotFoundException e) {
            System.out.println("No workload found");
        } catch (IOException e) {
            System.out.println("IO exception");
        }

        // Store scheme, algorithm and packet rate arguments.
        String scheme = args[0];
        String algo = args[1];
        Integer packetRate = Integer.parseInt(args[4]);

        // Polymorphic type searchAlgorithm which is instantiated based on what algorithm is provided.
        SearchAlgorithm searchAlgorithm = null;

        if( algo.equals("SDP") ) {
            searchAlgorithm = new SearchAlgorithm(graph, requests, scheme, packetRate, "SDP");
        } else if( algo.equals("LLP") ) {
            searchAlgorithm = new SearchAlgorithm(graph, requests, scheme, packetRate, "LLP");
        }
        // Default to shorted hop path if not specified.
        else {
            searchAlgorithm = new SearchAlgorithm(graph, requests, scheme, packetRate, "SHP");
        }

        // Execute the analyse method which does most of the work and produces a string which is the output.
        System.out.println(searchAlgorithm.analyseWithAlgorithm());

    }
    private static ArrayList<ConnectionRequest> requests;
}
