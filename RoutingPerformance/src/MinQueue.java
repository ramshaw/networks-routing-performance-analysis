import java.util.ArrayList;
import java.util.HashMap;


public class MinQueue {

    public MinQueue( ) {
        queue = new ArrayList<Vertex>();
        values = new HashMap<Vertex, Float>();
    }

    public boolean add( Vertex v, float val ) {
        int i = 0;
        values.put(v, val);
        if( queue.contains(v) ) queue.remove(v);

        while( queue.size() > i ) {
            if( values.get(queue.get(i)) > val ) break;
            else i++;
        }
        queue.add(i, v);
        return true;
    }

    public Vertex pop() {
        Vertex toRemove = queue.get(0);
        queue.remove(0);
        return toRemove;
    }

    public ArrayList<Vertex> getList() {
        return queue;
    }

    public Float getDistance(Vertex v) {
        return values.get(v);
    }

    public boolean isEmpty() {
        if( queue.size() == 0 ) return true;
        else return false;
    }

    ArrayList<Vertex> queue;
    HashMap<Vertex, Float> values;
}
