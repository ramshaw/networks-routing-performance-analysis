import java.util.*;


public class Graph {

    public Graph() {
        this.vertices = new ArrayList<Vertex>();
        this.edges = new HashMap<Vertex, ArrayList<Edge>>();
    }

    public void addEdge( String name, String next, String propDelay, String capacity ) {
        Vertex v = new Vertex(name);

        if( !vertices.contains(v) ) vertices.add(v);

        Vertex v1 = new Vertex(next);
        if( !vertices.contains(v1) ) vertices.add(v1);

        ArrayList<Edge> e = edges.get(v);
        Edge newEdge = new Edge(v, v1, Integer.parseInt(capacity), Float.parseFloat(propDelay));
        if( e == null ) e = new ArrayList<Edge>();
        if( ! e.contains( newEdge ) && !v1.equals(v)) e.add( newEdge );
        edges.put(v, e);

        Edge newEdgeB = new Edge( v1, v, Integer.parseInt(capacity), Float.parseFloat(propDelay));
        ArrayList<Edge> e1 = edges.get(v1);
        if( e1 == null ) e1 = new ArrayList<Edge>();
        if( ! e1.contains( newEdgeB ) && !v1.equals(v) ) e1.add(newEdgeB);
        edges.put(v1, e1);
    }

    public Vertex getVertex( char name ) {
        for (Vertex v: vertices) {
            if (v.getName() == name ) return v;
        }
        return null;
    }

    public ArrayList<Vertex> dijkstra (Vertex start, Vertex end, float startTime, float duration, String scheme) {
        ArrayList<Vertex> path = new ArrayList<Vertex>();
        HashMap<Vertex, Vertex> parent = new HashMap<Vertex, Vertex>();
        HashMap<Vertex, Boolean> visited = new HashMap<Vertex, Boolean>();
        Duration dur = new Duration(startTime, startTime + duration);

        // Initialisation
        MinQueue queue = new MinQueue();
        for (Vertex v: vertices) {
            visited.put(v, false);
            if( !v.equals(start) ) {
                queue.add(v, infinity);
            } else {
                queue.add(v, 0);
            }
        }

        // Main Dijkstra's algorithm
        Vertex prev = start;
        float prevCost = 0;
        while( !queue.isEmpty() ) {
            Vertex curr = queue.pop();
            visited.put(curr, true);
            prevCost = edgeLoadRatio( prev, curr, dur );
       //     System.out.println("  Edge delay of " + prev.getName() + " " + curr.getName() + " is " + prevCost );
            prev = curr;

            ArrayList<Edge> neighbours = edges.get(curr);
            for( Edge n: neighbours ) {
                Vertex v = n.getB();
                if( !visited.get(v) ) {
                    // Cost of each link / edge based on algorithm
                    float cost;
                    float newCost;

                    if ( scheme.equals("SDP") ){
                        cost = n.getDelay();
                        newCost = queue.getDistance(curr) + cost;
                    } else if ( scheme.equals("LLP") ) {
                        newCost = Math.max( edgeLoadRatio( v, curr, dur ) , prevCost );
                    } else {
                        cost = 1;
                        newCost = queue.getDistance(curr) + cost;
                    }

                    if( newCost < queue.getDistance(v) ) {
                        parent.put(v, curr);
                        queue.add(v, newCost);
                    }
                }
            }
        }


        // debug printing of path
        Vertex curr = end;
     //   System.out.println("Path from " + start.getName() + " to " + end.getName());
        while( parent.containsKey(curr) ) {
       //     System.out.println("    Node " + curr.getName());
            path.add(curr);
            curr = parent.get(curr);
        }
        path.add(curr);
      //  System.out.println("    Node " + curr.getName());

        // Check if path is blocked
        int i = 0;
        boolean pathAvailable = true;
        ArrayList<Edge> pathEdges = new ArrayList<Edge>();

        // Store path as a list of edges.
        Vertex a = null;
        Vertex b = null;
        while( i < path.size() ) {
            if( a == null ) {
                a = path.get(i);
            } else if( b == null ) {
                b = path.get(i);
                Edge e = new Edge( a, b, -1, -1);
                pathEdges.add(e);
                e = new Edge( b, a, -1, -1 );
                pathEdges.add(e);
                a = b;
                b = null;
            }
            i++;
        }

        // Check if there are clashes with existing connections
        for( Edge e: pathEdges ) {
            Vertex v1 = e.getA();
            ArrayList<Edge> existing = edges.get(v1);
            for( Edge ex: existing ) {
                if( ex.equals(e) ) {
                    if( !ex.isFree( dur ) ) {
                        pathAvailable = false;
                        break;
                    }
                }
            }
        }

        // If there are no clashes, addDuration to existing edges.
        if( pathAvailable ) {
            for (Edge e : pathEdges) {
                Vertex v1 = e.getA();
                for (Edge ex : edges.get(v1)) {
                    if (ex.equals(e) ) {
                        ex.addDuration(dur);
                    }
                }
            }
            // if pathAvailable, return path
            return path;
        }

        // If clashes, return null, connection failed.
        else return null;
    }

    public float edgeLoadRatio( Vertex a, Vertex b, Duration dur ) {
        ArrayList<Edge> e = edges.get(a);
        for( Edge ed: e ) {
            if( ed.getB().equals(b) ) {
                return ed.loadRatio(dur);
            }
        }
        return 0;
    }

    public float pathDelay( ArrayList<Vertex> path ) {
        int i = 0;
        float delay = 0;
        Vertex a = null;
        Vertex b = null;
        while( i < path.size() ) {
            if( a == null ) {
                a = path.get(i);
            } else if( b == null ) {
                b = path.get(i);
                Edge e = new Edge( a, b, -1, -1);
                ArrayList<Edge> existing = edges.get(a);
                for( Edge ed: existing ) {
                    if( ed.equals(e)) {
                        delay += ed.getDelay();
                    }
                }
                a = b;
                b = null;
            }
            i++;
        }
        return delay;
    }

    public void clean( float time ) {
        for( Vertex v: vertices ) {
            ArrayList<Edge> e = edges.get(v);
            for( Edge e1: e ) {
                e1.clean( time );
            }
        }
    }

    // define infinity to be largest integer.
    private static final Integer infinity = 999999999;
    private ArrayList<Vertex> vertices;
    private HashMap<Vertex, ArrayList<Edge>> edges;

}