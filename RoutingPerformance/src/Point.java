/**
 * Created by aaron on 29/10/14.
 */
public class Point {

    public Point( float time, int startFin) {
        this.time = time;
        this.startFin = startFin;
    }

    public float getTime() {
        return time;
    }

    float time;
    int startFin;
}
