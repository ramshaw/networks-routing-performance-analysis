/**
 * Created by aaron on 22/10/14.
 */
public class ConnectionRequest {

    public ConnectionRequest( String initTime, String start, String end, String duration) {
        this.initTime = Float.parseFloat(initTime);
        this.start = start.charAt(0);
        this.end = end.charAt(0);
        this.duration = Float.parseFloat(duration);
    }

    public float getInitTime() {
        return initTime;
    }

    public char getStart() {
        return start;
    }

    public char getEnd() {
        return end;
    }

    public float getDuration() {
        return duration;
    }

    private float initTime;
    private char start;
    private char end;
    private float duration;
}
