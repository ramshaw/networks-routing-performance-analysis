import sun.jvm.hotspot.utilities.Interval;

/**
 * Created by aaron on 25/10/14.
 */
public class Duration {

    public Duration( float start, float end ) {
        this.start = start;
        this.end = end;
    }

    public float getStart() {
        return start;
    }

    public float getEnd() {
        return end;
    }

    public boolean overlaps( Duration d ) {
        if( d.getStart() <= this.start && d.getEnd() > this.start ) return true;
        else if ( d.getStart() >= this.start && d.getStart() < this.end ) return true;
        else return false;
    }

    public void setEnd(float end) {
        this.end = end;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Duration duration = (Duration) o;

        if (Float.compare(duration.end, end) != 0) return false;
        if (Float.compare(duration.start, start) != 0) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (start != +0.0f ? Float.floatToIntBits(start) : 0);
        result = 31 * result + (end != +0.0f ? Float.floatToIntBits(end) : 0);
        return result;
    }

    private float start;
    private float end;
}
