import java.util.*;

public class Edge {

    public Edge( Vertex a, Vertex b, int capacity, float delay ) {
        this.a = a;
        this.b = b;
        this.capacity = capacity;
        this.timesOccupied = new ArrayList<Duration>();
        this.delay = delay;
    }

    public void addDuration( Duration d ) {

   //     System.out.println("    Booking edge " + a.getName() + " " + b.getName() + " times " + d.getStart() + " to " + d.getEnd() + " " + this.timesOccupied.size());
        boolean extended = false;
        for( Duration d1: timesOccupied ) {
            if( d1.getEnd() == d.getStart() ) {
                d1.setEnd(d.getEnd());
                extended = true;

            }
        }
        if( !extended ) {
            timesOccupied.add(d);
        }

     //   System.out.println(" Booked " + timesOccupied.size());
    }

    public Vertex getA() {
        return a;
    }

    public Vertex getB() {
        return b;
    }

    public float getDelay() {
        return this.delay;
    }

    public boolean isFree( Duration dur ) {
        int load = getLoad(dur );
    //    System.out.println("Max load is " + load);

        //  System.out.println("        Edge " + this.getA().getName() + " to " + this.getB().getName() + " Load = " + load + " / " + capacity);
        if( load >= capacity ) {
      //      System.out.println("Capacity fail" + load + " " + capacity);
            return false;
        }
        else return true;
    }

    public float loadRatio( Duration dur ) {
        float load = (float) getLoad(dur);
        return load/capacity;
    }

    public void clean( float time ) {
        for( int i = 0; i < timesOccupied.size(); i++ ) {
            if( timesOccupied.get(i).getStart() + timesOccupied.get(i).getEnd() < time ) {
                timesOccupied.remove(i);
            }
        }

    }

    public int getLoad( Duration dur ) {




        /*
        ArrayList<Float> points = new ArrayList<Float>();

        for( Duration d : list ) {
            points.add( d.getStart() );
            points.add( d.getEnd() );
        }

        ArrayList<Duration> newList = new ArrayList<Duration>();

        int i = 0;
        Float a = dur.getStart();
        Float b = null;
        while( i < points.size() ) {
           if( points.get(i) > dur.getStart() && points.get(i) < dur.getEnd() ) {
               if (a == null) {
                   a = points.get(i);
               } else {
                   b = points.get(i);
                   newList.add(new Duration(a, b));
                   a = null;
                   b = null;
               }
           }
           i++;
        }
        if( a != null ) {
            newList.add( new Duration(a, dur.getEnd() ) );
        }

        int load = 0;
        for( Duration d: newList ) {
            int curr = 0;
            for( Duration d1: list ) {
                if( d.overlaps(d1) ) curr++;
            }
            load = Math.max( curr, load );
        }
        return load;
        */

        /*
        ArrayList<Duration> clashes = new ArrayList<Duration>();
        for( Duration d: list ) {
            if( dur.overlaps(d) ) {
                clashes.add( d );
            }
        }
        int load = 999999999;
        for( int i = 0; i < clashes.size(); i++ ) {
            int c = 0;
            for( int j = 0; j < clashes.size(); j++ ) {
                if( j != i ) {
                    if( clashes.get(i).overlaps( clashes.get(j)) ) {
                        c++;
                    }
                }
            }
            if( c < load ) load = c;
        }
        return load; */



      //  KHAN's iterative solution
        ArrayList<Point> points = new ArrayList<Point>();
        //points.add(new Point(dur.getStart(), 0));
        //points.add(new Point(dur.getEnd(), 1));
      //   System.out.println("Finding overlaps " + dur.getStart() + " " + dur.getEnd() );
        for( int i = 0; i < timesOccupied.size(); i++ ) {
           if( (timesOccupied.get(i).getEnd() >= dur.getStart() ) ) {
      //          System.out.println("    " + timesOccupied.get(i).getStart() + "    " + timesOccupied.get(i).getEnd() );
                points.add(new Point(timesOccupied.get(i).getStart(), 0));
                points.add(new Point(timesOccupied.get(i).getEnd(), 1));
            } else {
                timesOccupied.remove(i);
            }
        }



        points.sort(new Comparator<Point>() {
            public int compare(Point p1, Point p2) {
                if( p1.getTime() > p2.getTime() ) {
                    return 1;
                } else if( p1.getTime() == p2.getTime() ) {
                    if( p1.startFin == 1 && p2.startFin == 0) {
                        return -1;
                    } else if( p1.startFin == 1 && p2.startFin == 1 ) {
                        return 0;
                    } else if( p1.startFin == 0 && p2.startFin == 0 ) {
                        return 0;
                    } else if( p1.startFin == 0  && p2.startFin == 1 ) {
                        return 1;
                    }
                }
                return -1;
            }
        });

        int c = 0;
        int overlap = 0;

        boolean end = false;
        Outer:
        for( Point p: points ) {

            if( p.startFin == 1 ) {
                if( p.getTime() <= dur.getStart() ) {
                    c--;
                } else if( p.getTime() <= dur.getEnd() ) {
                    c--;
                    overlap = Math.max(c,overlap);
                } else if( p.getTime() > dur.getEnd() ) {
                    overlap = Math.max(c,overlap);
                    end = true;
                    break Outer;
                }
            }

            else if( p.startFin == 0 ) {
                if( p.getTime() < dur.getStart() ) {
                    c++;
                }
                else if( p.getTime() < dur.getEnd() ) {
                    c++;
                    overlap = Math.max(c,overlap);
                }
                else if( p.getTime() >= dur.getEnd() ) {
                    overlap = Math.max(c, overlap);
                    end = true;
                    break Outer;
                }
            }
            /*
            if( p.getTime() > dur.getEnd() ) {
                break;
            }
            if (p.startFin == 0) c++;
            else if (p.startFin == 1) c--;

            if( p.getTime() >= dur.getStart() && p.getTime() <= dur.getEnd() ) {
                overlap = Math.max(c, overlap);
            }
*/


        }
        if( end != true ) {
            overlap = Math.max(c, overlap);
        }
    //    System.out.println("Overlap " + overlap);
        return overlap;

    }

    public boolean isDirectedEdge( Vertex a, Vertex b ) {
        if( this.a.equals(a) && this.b.equals(b) ) return true;
        else return false;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Edge edge = (Edge) o;
        if( a.equals(edge.a) && b.equals(edge.b) ) return true;
        if( a.equals(edge.b) && b.equals(edge.a) ) return true;
        return false;
    }

    @Override
    public int hashCode() {
        int result;
        if( a.getName() > b.getName() ) {
            result = a.hashCode();
            result = 31 * result + b.hashCode();
        }
        else {
            result = b.hashCode();
            result = 31 * result + a.hashCode();
        }
        return result;
    }

    private Vertex a;
    private Vertex b;
    private ArrayList<Duration> timesOccupied;
    private Integer capacity;
    private float delay;
}
